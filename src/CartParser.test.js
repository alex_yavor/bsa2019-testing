import CartParser from './CartParser';
import path from 'path';
import { readFileSync } from 'fs';
let parser, validate, parseLine, calcTotal;
const cartPath = path.resolve(__dirname, '../samples/cart.csv');
beforeEach(() => {
	parser = new CartParser();
	validate = parser.validate.bind(parser);
	parseLine = parser.parseLine.bind(parser)
	calcTotal = parser.calcTotal.bind(parser);
});

describe('CartParser - unit tests', () => {

	describe('parseLine', () => {
		it('should return object equals to csv string', () => {
			expect(parseLine('Tvoluptatem,10.32,1')).toEqual(
				{ id: expect.anything(Number), name: "Tvoluptatem", price: 10.32, quantity: 1 }
			)
		})
	})

	describe('validate', () => {
		it('should return error message in array when header has incorect name', () => {
			expect(validate(`name,Price,Quantity`))
				.toEqual([{ column: 0, message: "Expected header to be named \"Product name\" but received name.", row: 0, type: "header" }]);
			expect(validate(`Product name,Pric,Quantity`))
				.toEqual([{ column: 1, message: "Expected header to be named \"Price\" but received Pric.", row: 0, type: "header" }]);
			expect(validate(`Product name,Price,Qantity`))
				.toEqual([{ column: 2, message: "Expected header to be named \"Quantity\" but received Qantity.", row: 0, type: "header" }]);
		})

		const header = "Product name,Price,Quantity\n";

		it('should return error message in array when cells are less than expected', () => {
			expect(validate(`${header}Mollis consequat,9.00`)[0].message)
				.toEqual("Expected row to have 3 cells but received 2.");
			expect(validate(`${header}Mollis consequat`)[0].message)
				.toEqual("Expected row to have 3 cells but received 1.");
		})

		it('should return error message in array when cell received negative number', () => {
			expect(validate(`${header}Mollis consequat,-9.00,2`)[0].message)
				.toEqual("Expected cell to be a positive number but received \"-9.00\".");

			expect(validate(`${header}Mollis consequat,9.00,-2`)[0].message)
				.toEqual("Expected cell to be a positive number but received \"-2\".");
		})

		it('should return error message in array when header name is missing', () => {
			expect(validate(`
			Product name,Price
			Mollis consequat,9.00,2`)[0].message)
				.toEqual("Expected header to be named \"Quantity\" but received undefined.");
			expect(validate(`
			Product name, ,Quantity
			Mollis consequat,9.00,2`)[0].message)
				.toEqual("Expected header to be named \"Price\" but received .");
			expect(validate(`
				,Price,Quantity
				Mollis consequat,9.00,2`)[0].message)
				.toEqual("Expected header to be named \"Product name\" but received .");

		})

		it('should return empty array of errors when data is valid', () => {
			expect(validate(`
			${header}
			Mollis consequat,9.00,2
			Condimentum aliquet,13.90,1`))
				.toStrictEqual([]);
		})

		it('should return an array of length equals to number of invalid params', () => {
			expect(validate(`
			${header}Mollis consequat,2
			Condimentum aliquet,-13.90,2
			Scelerisque lacinia,18.90,-1
			Consectetur adipiscing,28.72`)).toHaveLength(4);

			expect(validate(`
			${header}Mollis consequat,2,1
			Condimentum aliquet,13.90,2
			Scelerisque lacinia,s,-1
			Consectetur adipiscing,28.72`)).toHaveLength(3);
		})


		it('should return row where was an error', () => {
			expect(validate(`${header}Mollis consequat,-9.00,2`)[0]).toHaveProperty('row', 1);
		})

		it('should return column where was an error', () => {
			expect(validate(`${header}Mollis consequat,-9.00,2`)[0]).toHaveProperty('column', 1);
		})

		it('should return type error', () => {
			expect(validate(`${header}Mollis consequat,-9.00,2`)[0]).toHaveProperty('type', 'cell');
			expect(validate(`${header}Mollis consequat,9.00`)[0]).toHaveProperty('type', 'row');
			expect(validate(`Product name,Price,Quantit`)[0]).toHaveProperty('type', 'header');
		})

	})

	describe('calcTotal', () => {
		it('shoud return calculated price of items', () => {
			const items = [
				{
					"price": 9,
					"quantity": 2
				},
				{
					"price": 10.32,
					"quantity": 1
				}]
			expect(calcTotal(items)).toEqual(28.32);
		})

	})
});

describe('CartParser - integration test', () => {
	describe('parse', () => {
		it('should return card equals to expected cart', () => {

			const jsonCart = parser.parse(cartPath);
			const expectedCartPath = path.resolve(__dirname, '../samples/cart.json');
			const expectedCart = JSON.parse(readFileSync(expectedCartPath, 'utf-8', 'r'));
			expectedCart["items"].forEach((item, index) => expectedCart.items[index].id = expect.anything(Number))
			expect(jsonCart).toEqual(expectedCart);
		})
	})

});